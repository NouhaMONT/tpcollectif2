package com.fr.isika.table8table8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Table8Application {

	public static void main(String[] args) {
		SpringApplication.run(Table8Application.class, args);
	}

}
